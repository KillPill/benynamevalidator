import asyncio

import uvicorn
from motor.motor_asyncio import AsyncIOMotorClient
from fastapi import FastAPI

import settings


app = FastAPI()
db = AsyncIOMotorClient(f'mongodb://{settings.MONGO_HOST}:27017', connect=False)
db.get_io_loop = asyncio.get_running_loop

from views import *


if __name__ == '__main__':
    uvicorn.run(
        'main:app',
        host='0.0.0.0',
        port=9090,
        reload=True
    )
