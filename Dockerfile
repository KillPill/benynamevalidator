FROM python:3.8.12-alpine

RUN apk update

ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt

COPY . /app
WORKDIR /app

ENTRYPOINT ["python3"]
CMD ["main.py"]
EXPOSE 9090