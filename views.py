from bson.objectid import ObjectId
from bson.errors import InvalidId

from starlette.requests import Request

from main import app, db
from utils import validate_competitor, name_without_numbers


@app.get('/validate')
async def validate_product(request: Request):
    params = request.query_params
    product_id = params.get('product_id')
    competitor_name = params.get('competitor_name')
    if not product_id or not competitor_name:
        return {'error': 'No data has been provided'}
    try:
        data = await db.beny.products.find_one({'_id': ObjectId(product_id)})
        if not data:
            return {'error': 'Product not found'}
        status, match_count, guestimate_status, guestimate_match = validate_competitor(data, competitor_name)
    except InvalidId:
        return {'error': 'Wrong product identifier'}
    return {
        'success': status,
        'match_count': match_count,
        'match_rate': round(match_count/len(name_without_numbers(data.get('name')).split(' ')) * 100, 2),
        'guestimate': {
            'status': guestimate_status,
            'match_rate': round(guestimate_match, 2)
        }
    }
