# Beny Name Validator

## Deployment:
* docker build -t beny_name_validator .
* docker run -dp 9090:9090 beny_name_validator

### Request (GET) params:
* product_id: str -> product id from DB
* competitor_name: str -> competitor's product name

### Response:
* success: bool -> validation status
* match_count: int -> count of similar words
* match_rate: float -> % of matching words
