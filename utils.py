import re
import difflib
from typing import Tuple


def name_without_numbers(name):
    """Removing ints for exact search"""
    return re.sub(r"[^a-zA-ZäÄüÜöÖß]+", ' ', name)


def validate_competitor(product: dict, competitor_name: str) -> Tuple[bool, float, bool, float]:
    amount_of_similar_words = 1
    try:
        if product.get('validate_name_wordCount', '-1') != '-1':
            amount_of_similar_words = min([int(product.get('validate_name_wordCount', 3)),
                                       max([1, float(
                                           len([i.strip() for i in name_without_numbers(product.get('name', '')).strip().split(' ')]) * float(
                                               product.get('validate_name_percentage', 0.7)))])])
        else:
            amount_of_similar_words = max([1, float(
                                           len([i.strip() for i in name_without_numbers(product.get('name', '')).strip().split(' ')]) * float(
                                               product.get('validate_name_percentage', 0.7)))])

    except ValueError:
        pass
    if product.get('validate_name_use_includes') and product.get('validate_name_includes') \
            and not all([name_without_numbers(word.lower().strip()) in name_without_numbers(competitor_name).lower().split(' ')
                        for word in product.get('validate_name_includes', [])]):
        return False, 0, False, 0
    if product.get('validate_name_use_excludes') and product.get('validate_name_excludes') \
            and [word for word in product.get('validate_name_excludes', [])
                 if name_without_numbers(word.lower().strip()) in name_without_numbers(competitor_name).lower().split(' ')]:
        return False, 0, False, 0
    calculated_amount = len(set(i.strip() for i in name_without_numbers(product.get('name', '')).lower().strip().split(' ')).intersection(
            set(i.strip() for i in name_without_numbers(competitor_name).lower().strip().split(' '))
        ))
    guestimate = difflib.SequenceMatcher(a=product.get('name'), b=competitor_name).ratio()
    return calculated_amount >= amount_of_similar_words, calculated_amount,\
           guestimate >= float(product.get('validate_name_percentage', 0.7)), guestimate
